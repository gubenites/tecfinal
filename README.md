# CSA

1° Passo: Configurar Mongo:
    > limpar a pasta data, que esta dentro de CSA\backend\back-junto\data
    > abra a pasta do mongo que esta no seu computador, na pasta bin abra um terminal e 
    digite o comando: mongod --dbpath C:\...CSA\backend\back-junto\data
    > adicionar os bancos de dados de receitas colheitas e usuarios que estao em um txt dentro da pasta CSA.
2° Passo: Configurar Node:
    > verificar o arquivo .env para conexão com o banco e o front.
    > dentro da pasta CSA\backend\back-junto abra um terminal e rode:
    > npm install dotenv-safe
    > npm start. isso caso seu node esteja instalado e atualizado.
3° Passo: Configurar React:
    > dentro da pasta CSA\front abra um terminal e rode:
    > npm install react
    > npm start. caso estaja com o react instalado e atualizado na sua maquina.
