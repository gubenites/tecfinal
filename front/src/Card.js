import React from 'react';
import classnames from 'classnames';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardHeader from '@material-ui/core/CardHeader';
import CardMedia from '@material-ui/core/CardMedia';
import CardContent from '@material-ui/core/CardContent';
import CardActions from '@material-ui/core/CardActions';
import Avatar from '@material-ui/core/Avatar';
import IconButton from '@material-ui/core/IconButton';
import Typography from '@material-ui/core/Typography';
import red from '@material-ui/core/colors/red';
import EditIcon from '@material-ui/icons/Edit';
import Logo from './img/bolo-de-cenoura.jpeg';
import Collapse from '@material-ui/core/Collapse';
import DeleteRoundedIcon from '@material-ui/icons/DeleteRounded';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import TextField from '@material-ui/core/TextField';
import AddIcon from '@material-ui/icons/Add';
import './Card.css';
import ReceitasJ from './receitasJ';
import NewReceita from './NewReceita';


const styles = theme => ({
  card: {
    maxWidth: 400,
  },
  media: {
    height: 0,
    paddingTop: '56.25%', // 16:9
  },
  actions: {
    display: 'flex',
  },
  expand: {
    transform: 'rotate(0deg)',
    transition: theme.transitions.create('transform', {
      duration: theme.transitions.duration.shortest,
    }),
    marginLeft: 'auto',
    [theme.breakpoints.up('sm')]: {
      marginRight: -8,
    },
  },
  expandOpen: {
    transform: 'rotate(180deg)',
  },
  avatar: {
    backgroundColor: red[500],
  },
});

class RecipeCard extends React.Component {
  state = {
    expanded0: false,
  };

  handleExpandClick0 = () => {
    this.setState(state => ({ expanded0: !state.expanded0 }));
  };

  handleChange = (event) => {
    this.setState({id: event.target.value});
 }
 handleSubmit = (event) => {
    //Make a network call somewhere
    event.preventDefault();
 }

  handleChange = name => event => {
    this.setState({
      [name]: event.target.value,
    });
  };

  render() {
    const { classes } = this.props;
    return (
      <div id="Pagina">
        <ReceitasJ/>
        <div id="Card0" className="Card">
          <Card className={classes.card} margin='normal'>
            <CardHeader
              avatar={
                <Avatar aria-label="Recipe" className={classes.avatar}>
                  R
                </Avatar>
              }
              action={
                <IconButton title="Delete" aria-label="Delete">
                  <DeleteRoundedIcon />
                </IconButton>
              }
              title="Usuário"
              subheader="Data DD/MM/AAAA"
            />
            <CardMedia
              className={classes.media}
              image={Logo}
              title="Paella dish"
            />
            <CardContent>
              <CardActions className={classes.actions} disableActionSpacing>
                <IconButton title="Edit" aria-label="Edit">
                  <EditIcon />
                </IconButton>
                <IconButton
                  className={classnames(classes.expand, {
                    [classes.expandOpen]: this.state.expanded0,
                  })}
                  onClick={this.handleExpandClick0}
                  aria-expanded={this.state.expanded0}
                  aria-label="Show more"
                > 
                <ExpandMoreIcon />
                </IconButton>
              </CardActions>
            </CardContent>
            <Collapse in={this.state.expanded0} timeout="auto" unmountOnExit>
              <ReceitasJ/>
            </Collapse>
          </Card>
        </div>
      </div>
    );
  }
}

RecipeCard.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(RecipeCard);