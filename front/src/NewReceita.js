import React, { Component } from 'react';
import './receitasJ.css'
import Axios from 'axios'
import Home from './Home'
import TextField from '@material-ui/core/TextField';
import IconButton from '@material-ui/core/IconButton';
import AddIcon from '@material-ui/icons/Add';
import Button from '@material-ui/core/Button';

class NewReceita extends Comment {

    constructor (props) {
        super(props)
        this.state={
            titulo: "",
            ingredientes: "",
            preparo: "",
            response: null,
            errorMessage: null
        }
    }

    handleInputChange = (e) => {

        this.setState({

            [e.target.name]: e.target.value

        });
    }

    handleSubmit = (e) => {

        e.preventDefault();

        const url = 'http://localhost:5000/recipes/new';

        const receita = {

            titulo: this.state.titulo, 
            ingredientes: this.state.ingredientes,
            preparo: this.state.preparo
        }

       Axios.post(url, receita).then(function(res){
       console.log('oi');
      });
      window.location = '/receitas';
    }

    altentication = async () =>{
        if (this.state.titulo === '' || this.state.ingredientes === '') {
            this.setState({ errorMessage: 'Por favor coloque a receita'})
        }
        else if (this.state.titulo === '' && this.state.ingredientes === '') {
            this.setState({ errorMessage: 'Por favor coloque a receita'})
        }

        try {
            let response = await fetch('/recipes/new', {
              method: 'POST',
              headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
              },
              body: JSON.stringify({
                titulo: this.state.titulo,
                ingredientes: this.state.ingredientes,
                preparo: this.state.preparo
               })
            })
          
            let responseJson = await response.json()
      
            console.log("fhsdgdfg");
            console.log(responseJson);
      
          } catch (error) {
            console.log(error)
        }
    }

    render() {
        return (
            <div className="root">     
              <form onSubmit={this.handleSubmit}>
                <div class="group">
                  <input id="receita" type="text" onChange={(ev) => {
                    this.setState({ titulo: ev.target.value })}}/><span class="highlight"></span><span class="bar"></span>
      
                  <label>titulo</label>
                </div>
                <div class="group">
                  <input id="ingrediente" type="text" onChange={(ev) => {
                    this.setState({ ingredientes: ev.target.value })}} /><span class="highlight"></span><span class="bar"></span>
                  <label>ingrediente</label>
                </div>
                <div class="group">
                  <input id="preparo" type="text" onChange={(ev) => {
                    this.setState({ preparo: ev.target.value })}} /><span class="highlight"></span><span class="bar"></span>
                  <label>preparo</label>
                </div>
                {/* <Link to="/"> */}
                  <Button type="submit" class="button buttonBlue" >
                    Registrar
                    <div class="ripples buttonRipples"><span class="ripplesCircle"></span></div>
                  </Button>
                {/* </Link> */}
                { this.state.errorMessage }
              </form>
            </div>
          );
    }
}
export default NewReceita;