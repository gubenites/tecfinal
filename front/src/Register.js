import React, { Component } from 'react';
import './Register.css'
import logo from './img/csa-logo-p.png'
import Button from '@material-ui/core/Button';
import { Link } from "react-router-dom";
import Axios from 'axios'
import Home from './Home'

class Register extends React.Component {

  constructor(props){
    super(props)
    this.state={
      login: "",
      senha: "",
      passw:"",
      _id: "",
      response: null,
      errorMessage: null
    }
  }

  handleInputChange = (e) => {

        this.setState({

            [e.target.name]: e.target.value

        });
    }

    handleSubmit = (e) => {

        e.preventDefault();

        const url = 'http://localhost:5000/register';

        const user = {

            login: this.state.login, 
            senha: this.state.senha,
            passw: this.state.passw
        }

       Axios.post(url, user).then(function(res){
       console.log('oi');
      });
      window.location = '/';


  }
  login = async () => {
    if (this.state.login === '' || this.state.senha === '') {
      this.setState({ errorMessage: 'Por favor coloque seu usuario e sua senha'})
    }

    else if (this.state.senha === '' && this.state.login === '') {
      this.setState({ errorMessage: 'Por favor coloque seu usuario e sua senha'})
    }

    try {
      let response = await fetch('/register', {
        method: 'POST',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json'
        },
        body: JSON.stringify({
          login: this.state.login,
          senha: this.state.senha,
          passw: this.state.passw
         })
      })
    
      let responseJson = await response.json()

      console.log("fhsdgdfg");
      console.log(responseJson);

      if (responseJson._id) {
        this.props.onLogin(responseJson)
        this.props.history.push('/')
      }

      else if (responseJson._id == null) {
        this.setState({ errorMessage: 'Usuário ou senha inválidos'})
      }

    } catch (error) {
      console.log(error)
    }    
  }

  render() {
    return (
      <div className="root">
        <hgroup>
            <h1>CSA</h1>
            <img src={logo} alt="logo"></img>
        </hgroup>

        <form onSubmit={this.handleSubmit}>
          <div class="group">
            <input id="register" type="text" onChange={(ev) => {
              this.setState({ login: ev.target.value })}}/><span class="highlight"></span><span class="bar"></span>

            <label>Novo Usuário</label>
          </div>
          <div class="group">
            <input id="senha" type="text" onChange={(ev) => {
              this.setState({ senha: ev.target.value })}} /><span class="highlight"></span><span class="bar"></span>
            <label>Senha</label>
          </div>
          <div class="group">
            <input id="senhaConfirma" type="text" onChange={(ev) => {
              this.setState({ passw: ev.target.value })}} /><span class="highlight"></span><span class="bar"></span>
            <label>Confirmar Senha</label>
          </div>
          {/* <Link to="/"> */}
            <Button type="submit" class="button buttonBlue" >
              Registrar
              <div class="ripples buttonRipples"><span class="ripplesCircle"></span></div>
            </Button>
          {/* </Link> */}
          { this.state.errorMessage }
        </form>
      </div>
    );
  }
}

export default Register;

