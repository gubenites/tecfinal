import React, { Component } from 'react';
import './receitasJ.css'
import Typography from '@material-ui/core/Typography';
import CardContent from '@material-ui/core/CardContent'; 
import Axios from 'axios'
import Button from '@material-ui/core/Button';


const ListIngredientes = () =>{
    return(
        <Typography component="p">
            <ul>
                <li>3 cenouras médias raspadas e picadas, 3 ovos, 1 xícara de óleo</li>        
            </ul>
        </Typography>
        
    )
}

const ModoPreparo = () =>{
    return(
        <CardContent>
                <Typography paragraph>Method:</Typography>
                <Typography paragraph>
                  Heat 1/2 cup of the broth in a pot until simmering, add saffron and set aside for 10
                  minutes.
                </Typography>
                <Typography paragraph>
                  Heat oil in a (14- to 16-inch) paella pan or a large, deep skillet over medium-high
                  heat. Add chicken, shrimp and chorizo, and cook, stirring occasionally until lightly
                  browned, 6 to 8 minutes. Transfer shrimp to a large plate and set aside, leaving
                  chicken and chorizo in the pan. Add pimentón, bay leaves, garlic, tomatoes, onion,
                  salt and pepper, and cook, stirring often until thickened and fragrant, about 10
                  minutes. Add saffron broth and remaining 4 1/2 cups chicken broth; bring to a boil.
                </Typography>
                <Typography paragraph>
                  Add rice and stir very gently to distribute. Top with artichokes and peppers, and cook
                  without stirring, until most of the liquid is absorbed, 15 to 18 minutes. Reduce heat
                  to medium-low, add reserved shrimp and mussels, tucking them down into the rice, and
                  cook again without stirring, until mussels have opened and rice is just tender, 5 to 7
                  minutes more. (Discard any mussels that don’t open.)
                </Typography>
                <Typography>
                  Set aside off of the heat to let rest for 10 minutes, and then serve.
                </Typography>
              </CardContent>
        
    )
}

class ReceitasJ extends Component {

  constructor (props) {
    super(props)
    this.state={
        titulo: "",
        ingredientes: "",
        preparo: "",
        response: null,
        errorMessage: null
    }
  }
  
  

  handleInputChange = (e) => {

    this.setState({

        [e.target.name]: e.target.value

    });
  }


  
    handleSubmit = (e) => {

        e.preventDefault();

        const url = 'http://localhost:5000/recipes/new';

        const receita = {

            titulo: this.state.titulo, 
            ingredientes: this.state.ingredientes,
            preparo: this.state.preparo
        }

       Axios.post(url, receita).then(function(res){
       console.log('oi');
      });
      window.location = '/receitas';
    }
    
    recipes = async() => {

      if (this.state.titulo === '' || this.state.ingredientes === '') {
        this.setState({ errorMessage: 'Por favor coloque a receita'})
      }
      else if (this.state.titulo === '' && this.state.ingredientes === '') {
        this.setState({ errorMessage: 'Por favor coloque a receita'})
      }
        try {
            let response = await fetch('/recipes', {
              method: 'POST',
              headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
              },
              body: JSON.stringify({
                titulo: this.state.titulo,
                ingredientes: this.state.ingredientes,
                preparo: this.state.preparo
               })
            })
          
            let responseJson = await response.json()
      
            console.log("fhsdgdfg");
            console.log(responseJson);
      
          } catch (error) {
            console.log(error)
        }
    }

    render(){
        return(
            
          <div className="root">     
          <form onSubmit={this.handleSubmit}>
            <div class="group">
              <input id="receita" type="text" onChange={(ev) => {
                this.setState({ titulo: ev.target.value })}}/><span class="highlight"></span><span class="bar"></span>
  
              <label>titulo</label>
            </div>
            <div class="group">
              <input id="ingrediente" type="text" onChange={(ev) => {
                this.setState({ ingredientes: ev.target.value })}} /><span class="highlight"></span><span class="bar"></span>
              <label>ingrediente</label>
            </div>
            <div class="group">
              <input id="preparo" type="text" onChange={(ev) => {
                this.setState({ preparo: ev.target.value })}} /><span class="highlight"></span><span class="bar"></span>
              <label>preparo</label>
            </div>
            {/* <Link to="/"> */}
              <Button type="submit" class="button buttonBlue" >
                Nova receita
                <div class="ripples buttonRipples"><span class="ripplesCircle"></span></div>
              </Button>
            {/* </Link> */}
            { this.state.errorMessage }
          </form>
        </div>
            
        )
    }
}

export default ReceitasJ;